#!/bin/bash
set -e
cd dok/
pwd
cd ../
pwd
pip install Django==1.6.1
pip install gunicorn==19.9.0
python manage.py syncdb
gunicorn dok.wsgi -b 0.0.0.0:8000
