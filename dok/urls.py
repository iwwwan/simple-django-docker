from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dok.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admins/', include(admin.site.urls)),
)
